# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Post.create(name: 'JOSH CONSTINE', title: 'Facebook Releases iOS 6 SDK', content: 'Facebook has just released the Facebook SDK 3.1 for iOS which lets app developers on its platform to take advantage of single sign-on through native login and other features from the social network getting baked into iOS 6.')
Post.create(name: 'DARRELL ETHERINGTON', title: 'The Sweet, Sweet Cruelty Of Apple', content: 'Apples new iPhone 5 comes with a completely new connector, a first since the introduction of the 30-pin model alongside the 3rd generation iPod (though that version received updates including video capabilities along the way). ')
Post.create(name: 'GREGORY FERENSTEIN', title: 'MIT Hacks Kinect Laser', content: 'When most of us play video games, our greatest accomplishment is the mound of Cheetos dust we can collect in a single sitting.')

