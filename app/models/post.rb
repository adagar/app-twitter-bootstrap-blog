class Post < ActiveRecord::Base
  attr_accessible :content, :name, :title
  validates_presence_of :name, :content	
  validates_presence_of :title, :length => { :minimum => 5 }
end
